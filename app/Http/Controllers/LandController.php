<?php

namespace App\Http\Controllers;

use App\Models\land;
use App\Http\Requests\StorelandRequest;
use App\Http\Requests\UpdatelandRequest;
use Illuminate\Http\Request;
class LandController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
       //$lands = land::all();
       $lands = land::paginate(12);
       
        return view('page.land.index', compact('lands'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('page.land.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\StorelandRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
         $request->validate([
            "libelle" => "required|unique:lands,libelle",
            "code_indicatif" => "required",
            "continent" => "required|string",
            "capitale" => "required|string",
            "population" => "required|numeric",
            "superficie" => "required|numeric",
            "monnaie" => "required",
            "langue" => "required",
            "est_laique" => "required"
        ]);

        land::create($request->all());
        return redirect()->route('land.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\land  $land
     * @return \Illuminate\Http\Response
     */
    public function show(land $land)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\land  $land
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $lands = Land::findOrFail($id);
        return view("page.land.edit", compact('lands'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\StorelandRequest  $request
     * 
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $lan =$request->validate([
            "libelle" => "required|unique:lands,libelle",
            "code_indicatif" => "required",
            "description" => "required",
            "continent" => "required|string",
            "capitale" => "required|string",
            "population" => "required|numeric",
            "superficie" => "required|numeric",
            "monnaie" => "required",
            "langue" => "required",
            "est_laique" => "required"
        ]);
        //$land = land::find($id);
         land::whereId($id)->update($lan);

       
        return redirect()->route('land.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\land  $land
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        land::find($id)->delete();
        return redirect()->route('land.index');
    }
}
