<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('lands', function (Blueprint $table){
            $table->id();
            $table->string("libelle");
            $table->text("description")->nullable;
            $table->string("code_indicatif")->unique();
            $table->enum('continent', ['Afrique', 'Amerique', 'Asie', 'Europe','Oceanie']);
            $table->integer("population");
            $table->string("capitale");
            $table->enum('monnaie', ['XOF', 'EUR', 'DOLLAR']);
            $table->enum('langue', ['FR', 'EN', 'AR', 'ES']);
            $table->integer("superficie");
            $table->boolean("est_laique")->default(true);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('lands');
    }
};
