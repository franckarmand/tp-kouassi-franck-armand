<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\land>
 */
class LandFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition()
    {
	

        return [
            'libelle' => $this->faker->country(),
            'description'=> $this->faker->paragraph(1),
            'code_indicatif' => $this->faker->numerify('+ ###'),
            "continent"=>$this->faker->randomElement(['Afrique', 'Amerique', 'Asie', 'Europe','Oceanie']),
            "population"=>rand(1000,10000000),
            "capitale"=>$this->faker->city(),
            "monnaie"=>$this->faker->randomElement(['XOF', 'EUR', 'DOLLAR']),
            "langue"=>$this->faker->randomElement(['FR', 'EN', 'AR', 'ES']),
            "superficie"=>rand(3250,215423),
            "est_laique"=>$this->faker->boolean()
        ];
    }
}
