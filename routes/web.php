<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\LandController;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//      return view('welcome');
// });
Route::view("","layouts.main");
Route::get('/land', [LandController::class, 'index'])->name('land.index');
Route::get('/land/create', [LandController::class, 'create'])->name('land.create');
Route::post('/land', [LandController::class, 'store'])->name('land.valider');
Route::get('/land/edit{id}', [LandController::class, 'edit'])->name('land.edit');
Route::post('/land/update{id}', [LandController::class, 'update'])->name('land.update');
Route::get('/land/delete{id}', [LandController::class, 'destroy'])->name('land.sup');