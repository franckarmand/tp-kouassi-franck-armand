@extends('layouts.main')

@section('content')
    <div class="row m-lg-3">
        <div class="col-md-12">
            <!-- general form elements -->
            <div class="card card-primary">
                <div class="card-header">
                    <h3 class="card-title">Formulaire de modification</h3>
                </div>
                <!-- /.card-header -->
                <!-- form start -->
                <form action="{{ route('land.update', ["id" => $lands->id]) }}"  method="POST">
                    @csrf
                    @method('POST')
                    <div class="card-body">
                        <div class="form-group">
                            <label for="libelle">Pays</label>
                            <input type="text" class="form-control" name="libelle" placeholder="Entrez un pays" value="{{ $lands->libelle }}">
                        </div>
                        <div class="form-group">
                            <label for="capitale">Capitale</label>
                            <input type="text" class="form-control" name="capitale" placeholder="Entrez la capitale" value="{{ $lands->capitale }}">
                        </div>
                        <div class="form-group">
                            <label for="description">Description</label>
                            <input type="text" class="form-control" name="description" placeholder="Entrez une description" value="{{ $lands->description }}">
                        </div>
                        <div class="form-group">
                            <label for="code_indicatif">Code indicatif</label>
                            <input type="text" class="form-control" name="code_indicatif"
                                placeholder="Entrez le code indicatif du pays" value="{{ $lands->code_indicatif }}">
                        </div>
                        <div class="form-group">
                            <label for="population">Population</label>
                            <input type="text" class="form-control" name="population"
                                placeholder="Entrez le nombre d'habitants" value="{{ $lands->population}}">
                        </div>
                        <div class="form-group">
                            <label for="superficie">Superficie</label>
                            <input type="text" class="form-control" name="superficie"
                                placeholder="Entrez la superficie du pays" value="{{ $lands->superficie }}">
                        </div>
                        <div class="row">
                            <div class="col-sm-6">
                                <!-- select -->
                                <div class="form-group">
                                    <label>Continent</label>
                                    <select class="form-control" name="continent" >
                                        <option value="{{ $lands->continent }}" selected hidden>{{ $lands->continent }}</option>
                                        <option value="Afrique">Afrique</option>
                                        <option value="Asie">Asie</option>
                                        <option value="Antartique">Antartique</option>
                                        <option value="Amerique">Amérique</option>
                                        <option value="Europe">Europe</option>
                                        <option value="Oceanie">Océanie</option>
                                    </select>
                                </div>
                            </div>

                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label>Monnaie</label>
                                    <select class="form-control" name="monnaie" >
                                        <option value="{{ $lands->monnaie }}" selected hidden>{{ $lands->monnaie }}</option>
                                        <option value="EUR">EUR</option>
                                        <option value="DOLLAR">DOLLAR</option>
                                        <option value="XOF">XOF</option>
                                    </select>
                                </div>
                            </div>

                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label>Langue</label>
                                    <select class="form-control" name="langue">
                                        <option value="{{ $lands->langue }}" selected hidden>{{ $lands->langue }}</option>
                                        <option value="FR">FR</option>
                                        <option value="EN">EN</option>
                                        <option value="AR">AR</option>
                                        <option value="ES">ES</option>
                                    </select>
                                </div>
                            </div>

                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label>Est Laique</label>
                                    <select class="form-control" name="est_laique" >
                                        <option  value="{{ ($lands->est_laique)?"1":"0" }}" selected hidden>{{ ($lands->est_laique)?"OUI":"NON" }}</option>
                                        <option value="1">OUI</option>
                                        <option value="0">NON</option>
                                    </select>
                                </div>
                            </div>
                        </div>

                    </div>

                    <div class="card-footer">
                        <button type="submit" class="btn btn-primary">Modifier</button>
                    </div>
                </form>
            </div>
            <!-- /.card -->
        </div>
    </div>
@endsection
