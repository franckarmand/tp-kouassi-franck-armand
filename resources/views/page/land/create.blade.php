@extends('layouts.main')

@section('content')
    <div class="row m-lg-3">
        <div class="col-md-12">
            <!-- general form elements -->
            <div class="card card-primary">
                <div class="card-header">
                    <h3 class="card-title">Formulaire d'enregistrement</h3>
                </div>
                <!-- /.card-header -->
                <!-- form start -->
                <form action="{{ route('land.valider') }}" method="POST">
                    @csrf
                    @method('POST')
                    <div class="card-body">
                        <div class="form-group">
                            <label for="libelle">Pays</label>
                            <input type="text" class="form-control" name="libelle" placeholder="Entrez un pays">
                        </div>
                        <div class="form-group">
                            <label for="capitale">Capitale</label>
                            <input type="text" class="form-control" name="capitale" placeholder="Entrez la capitale">
                        </div>
                        <div class="form-group">
                            <label for="description">Description</label>
                            <input type="text" class="form-control" name="description"
                                placeholder="Entrez une description">
                        </div>
                        <div class="form-group">
                            <label for="code_indicatif">Code indicatif</label>
                            <input type="text" class="form-control" name="code_indicatif"
                                placeholder="Entrez le code indicatif du pays">
                        </div>
                        <div class="form-group">
                            <label for="population">Population</label>
                            <input type="text" class="form-control" name="population"
                                placeholder="Entrez le nombre d'habitants">
                        </div>
                        <div class="form-group">
                            <label for="superficie">Superficie</label>
                            <input type="text" class="form-control" name="superficie"
                                placeholder="Entrez la superficie du pays">
                        </div>
                        <div class="row">
                            <div class="col-sm-6">
                                <!-- select -->
                                <div class="form-group">
                                    <label>Continent</label>
                                    <select class="form-control" name="continent">
                                        <option value="" selected hidden>Selectionner un continent</option>
                                        <option value="Afrique">Afrique</option>
                                        <option value="Asie">Asie</option>
                                        <option value="Antartique">Antartique</option>
                                        <option value="Amerique">Amérique</option>
                                        <option value="Europe">Europe</option>
                                        <option value="Oceanie">Océanie</option>
                                    </select>
                                </div>
                            </div>

                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label>Monnaie</label>
                                    <select class="form-control" name="monnaie">
                                        <option value="" selected hidden>Selectionner une monnaie</option>
                                        <option value="EUR">EUR</option>
                                        <option value="DOLLAR">DOLLAR</option>
                                        <option value="XOF">XOF</option>
                                    </select>
                                </div>
                            </div>

                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label>Langue</label>
                                    <select class="form-control" name="langue">
                                        <option value="" selected hidden>Selectionner une langue</option>
                                        <option value="FR">FR</option>
                                        <option value="EN">EN</option>
                                        <option value="AR">AR</option>
                                        <option value="ES">ES</option>
                                    </select>
                                </div>
                            </div>

                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label>Est laique</label>
                                    <select class="form-control" name="est_laique">
                                        <option value="" selected hidden>Selectionner une religion</option>
                                        <option value="1">OUI</option>
                                        <option value="0">NON</option>
                                    </select>
                                </div>
                            </div>
                        </div>

                    </div>

                    <div class="card-footer">
                        <button type="submit" class="btn btn-primary">Enregistrer</button>
                    </div>
                </form>
            </div>
            <!-- /.card -->
        </div>
    </div>
@endsection
