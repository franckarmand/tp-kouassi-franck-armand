@extends('layouts.main')
@section('content')
<div class="card">
    <div class="card-header">
      <h3 class="card-title">Liste des lands</h3>
    </div>
    <!-- /.card-header -->
    <div class="card-body">
      <table class="table table-bordered">
        <thead style="text-align:center">
          <tr>
	    <th>#</th>
            <th>Pays</th>
            <th class="w-100 p-3">Description</th>
            <th>Code indicatif</th>
            <th>Continent</th>
            <th>Population</th>
            <th>Capitale</th>
            <th>Monnaie</th>
            <th>Langue</th>
            <th>Superficie</th>
            <th>Laique</th>
            <th colspan="2">Actions</th>
          </tr>
        </thead>
        <tbody>
            @foreach ( $lands as $land)

            <tr>
                <td>{{ $land->id }}</td>
                <td>{{ $land->libelle }}</td>
               {{-- <td class="card-body table-responsive p-0" style="height: 100px;">{{ $land->description }}</td>--}}
		            <td class="text-wrap">{{ $land->description }}</td>
                <td>{{ $land->code_indicatif }}</td>
                <td>{{ $land->continent }}</td>
                <td>{{ $land->population }}</td>
                <td>{{ $land->capitale }}</td>
                <td>{{ $land->monnaie }}</td>
                <td>{{ $land->langue }}</td>
                <td>{{ $land->superficie }} km2</td>
                <td class="text-capitalize">{{ ($land->est_laique) ?"OUI":"NON"}}</td>

                <td>
                  <a href="{{route("land.edit", ["id" => $land->id])}}"><button class="btn btn-primary btn-circle" type="button"><i class="fa fa-edit"></i></button></a> </td>
                <td>   <a href="{{route("land.sup", ["id" => $land->id])}}"><button class="btn btn-danger btn-circle" type="button"><i class="fa fa-trash"></i></button></a>

        </td>

        @endforeach
        </tbody>
      </table> <br/><br/>
      {{-- Pagination --}}
      <div class="d-flex justify-content-center">
        {!! $lands->links() !!}
    </div>
    </div>
    <!-- /.card-body -->

  </div>
@endsection